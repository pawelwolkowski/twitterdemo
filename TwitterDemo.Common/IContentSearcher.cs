﻿using System.Threading.Tasks;
using TwitterDemo.Common.DTO;

namespace TwitterDemo.Common
{
    public interface IContentSearcher
    {
        Task<SearchContent> GetContent(string searchText);
        Task<SearchContent> GetContent(string searchText, string nextToken, string newestId = null, bool previous = false);
    }
}
