﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TwitterDemo.Common.DTO;

namespace TwitterDemo.Common
{
    public interface IContentFeeder
    {
        Task<List<FeederContent>> GetContent();
    }
}