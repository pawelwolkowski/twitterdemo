﻿using System;

namespace TwitterDemo.Common.DTO
{
    public class FeederContent
    {
        public string Text { get; set; }
        public string Created { get; set; }
    }
}
