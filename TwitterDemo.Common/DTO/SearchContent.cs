﻿using System;
using System.Collections.Generic;

namespace TwitterDemo.Common.DTO
{
    public class SearchContent
    {
        public List<Content> Contents { get; set; }
        public bool PreviousActive { get; set; } = false;
        public bool NextActive { get; set; } = false;
        public bool PreviousFirstTime { get; set; } = true;
        public string NewestId { get; set; }
        public string NextToken { get; set; }
    }

    public class Content
    {
        public string Text { get; set; }
        public string Author { get; set; }
        public string Created { get; set; }
    }
}
