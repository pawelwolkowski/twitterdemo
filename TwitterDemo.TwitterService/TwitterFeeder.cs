﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Humanizer;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using TwitterDemo.Common;
using TwitterDemo.Common.DTO;
using TwitterDemo.TwitterService.Models;

namespace TwitterDemo.TwitterService
{
    public class TwitterFeeder : IContentFeeder
    {
        private readonly IConfiguration _configuration;

        public TwitterFeeder(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<List<FeederContent>> GetContent()
        {
            var client = new RestClient("https://api.twitter.com/2/")
            {
                Authenticator = new JwtAuthenticator(_configuration.GetValue<string>("BearerToken"))
            };
            var userId = _configuration.GetValue<string>("HomeContentUserId");
            var tweetCount = _configuration.GetValue<int>("HomeContentCount");
            var request = new RestRequest(
                $"users/{userId}/tweets?tweet.fields=created_at&user.fields=created_at&max_results={tweetCount}",
                DataFormat.Json);
            var response = await client.GetAsync<TimeLineResponse>(request);
            return MapTo(response);
        }

        private List<FeederContent> MapTo(TimeLineResponse response)
        {
            return response.data.Select(s => new FeederContent {
                    Text = s.text, 
                    Created = $"{DateTimeFormater.AddOrdinal(s.created_at.Day)} {s.created_at:MMMM} {s.created_at:yyyy} at {s.created_at:h:mm tt}"})
                .ToList();
        }
    }
}