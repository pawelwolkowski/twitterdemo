﻿using System;
using System.Collections.Generic;

namespace TwitterDemo.TwitterService.Models
{
    public class TwitterSearchResult
    {
        public List<Datum> data { get; set; }
        public Meta meta { get; set; }
    }

    public class Mention
    {
        public int start { get; set; }
        public int end { get; set; }
        public string username { get; set; }
    }

    public class Entities
    {
        public List<Mention> mentions { get; set; }
    }

    public class Datum
    {
        public Entities entities { get; set; }
        public DateTime created_at { get; set; }
        public string id { get; set; }
        public string text { get; set; }
        public string author_id { get; set; }
    }
    
    public class Meta
    {
        public string newest_id { get; set; }
        public string oldest_id { get; set; }
        public int result_count { get; set; }
        public string next_token { get; set; }
    }
}
