﻿using System;
using System.Collections.Generic;

namespace TwitterDemo.TwitterService.Models
{
    public class TimeLineResponse
    {
        public List<HomeTweet> data { get; set; }
    }

    public class HomeTweet
    {
        public DateTime created_at { get; set; }
        public string id { get; set; }
        public string text { get; set; }
    }
}
