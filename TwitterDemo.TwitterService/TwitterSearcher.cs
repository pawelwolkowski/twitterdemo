﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using TwitterDemo.Common;
using TwitterDemo.Common.DTO;
using TwitterDemo.TwitterService.Models;

namespace TwitterDemo.TwitterService
{
    public class TwitterSearcher : IContentSearcher
    {
        private readonly IConfiguration _configuration;
        private int _resultCount;
        private string _bearerToken;

        public TwitterSearcher(IConfiguration configuration)
        {
            _configuration = configuration;
            _resultCount = _configuration.GetValue<int>("SearchContentCount");
            _bearerToken = _configuration.GetValue<string>("BearerToken");
        }

        public async Task<SearchContent> GetContent(string searchText)
        {
            return await GetContent(searchText, null);
        }

        public async Task<SearchContent> GetContent(string searchText, string nextToken, string newestId=null, bool previous=false)
        {
            var client = new RestClient("https://api.twitter.com/2/")
            {
                Authenticator = new JwtAuthenticator(_bearerToken)
            };
            var resourceLink =
                $"tweets/search/recent?query={searchText}&expansions=entities.mentions.username,author_id&tweet.fields=created_at&max_results={_resultCount}";
            if (!string.IsNullOrWhiteSpace(nextToken))
            {
                resourceLink += $"&next_token={nextToken}";
            }
            if (!string.IsNullOrWhiteSpace(newestId))
            {
                resourceLink += $"&since_id={newestId}";
            }
            var request = new RestRequest(resourceLink, DataFormat.Json);
            var response = await client.GetAsync<TwitterSearchResult>(request);
            return new SearchContent
            {
                Contents = response.data != null ? MapTo(response.data) : new List<Content>(),
                NextToken = response.meta.next_token,
                NewestId = previous ? newestId : response.meta.newest_id
            };
        }

        private List<Content> MapTo(List<Datum> datums)
        {
            var data = datums.Select(
                    s => new Content
                    {
                        Text = s.text,
                        Author = s.entities?.mentions.FirstOrDefault()?.username,
                        Created = $"{DateTimeFormater.AddOrdinal(s.created_at.Day)} {s.created_at:MMMM} {s.created_at:yyyy} at {s.created_at:h:mm tt}"})
                .ToList();
            return data;
        }
    }
}
