﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using TwitterDemo.Common;
using TwitterDemo.Common.DTO;

namespace TwitterDemo.Logic
{
    public class ContentService : IContentService
    {
        private readonly IContentFeeder _contentFeeder;
        private readonly IContentSearcher _contentSearcher;

        public ContentService(
            IContentFeeder contentFeeder,
            IContentSearcher contentSearcher)
        {
            _contentFeeder = contentFeeder;
            _contentSearcher = contentSearcher;
        }

        public async Task<List<FeederContent>> GetHomeContent()
        {
            var result = await _contentFeeder.GetContent();
            return result;
        }

        public async Task<SearchContent> GetSearchResult(string searchedText)
        {
            var searchedContent = await _contentSearcher.GetContent(
                searchedText);
            if (!string.IsNullOrWhiteSpace(searchedContent.NextToken)) searchedContent.NextActive = true;
            searchedContent.PreviousFirstTime = true;
            return searchedContent;
        }

        public async Task<SearchContent> GetNextSearchResult(string searchedText, string nextToken)
        {
            var searchedContent = await _contentSearcher.GetContent(
                searchedText,
                nextToken);
            if (!string.IsNullOrWhiteSpace(searchedContent.NextToken)) searchedContent.NextActive = true;
            searchedContent.PreviousActive = true;
            searchedContent.PreviousFirstTime = true;
            return searchedContent;
        }

        public async Task<SearchContent> GetPreviousSearchResult(string searchedText, string nextToken, string newestId, bool previousFirstTime)
        {
            var token = nextToken;
            if (previousFirstTime) token = string.Empty;

            var searchedContent = await _contentSearcher.GetContent(
                searchedText,
                token,
                newestId,
                true);
            searchedContent.NextActive = true;
            searchedContent.PreviousFirstTime = false;
            if (!string.IsNullOrWhiteSpace(searchedContent.NextToken)) searchedContent.PreviousActive = true;
            return searchedContent;
        }
    }

    public interface IContentService
    {
        Task<List<FeederContent>> GetHomeContent();
        Task<SearchContent> GetNextSearchResult(string searchedText, string nextToken);
        Task<SearchContent> GetPreviousSearchResult(string searchedText, string nextToken, string newestId, bool previousFirstTime);
        Task<SearchContent> GetSearchResult(string searchedText);
    }
}
