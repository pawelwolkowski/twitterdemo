﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TwitterDemo.Common.DTO;

namespace TwitterDemo.Web.Models
{
    public class HomeViewModel
    {
        public List<FeederContent> FeederContents { get; set; }
        public SearchContent SearchContent { get; set; }
        [Required]
        [MaxLength(25)]
        public string SearchedText { get; set; }

        public HomeViewModel()
        {
            SearchContent = new SearchContent();
        }
    }
}
