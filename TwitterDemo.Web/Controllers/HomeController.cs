﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TwitterDemo.Logic;
using TwitterDemo.Web.Models;

namespace TwitterDemo.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IContentService _contentService;

        [BindProperty]
        public HomeViewModel Model { get; set; }

        public HomeController(IContentService contentService)
        {
            _contentService = contentService;
            Model = new HomeViewModel();  
        }

        public async Task<IActionResult> Index()
        {
            var vm = new HomeViewModel
            {
                FeederContents = await _contentService.GetHomeContent()
            };
            return View(vm);
        }

        public async Task<IActionResult> Search()
        {
            var vm = new HomeViewModel
            {
                FeederContents = await _contentService.GetHomeContent()
            };
            if (!ModelState.IsValid) return View("Index", vm);
            var result = await _contentService.GetSearchResult(Model.SearchedText);
            result.PreviousFirstTime = false;
            vm.SearchContent = result;
            return View("Index", vm);
        }

        public async Task<IActionResult> Previous()
        {
            var nextToken = Request.Form["next_token"];
            var newestId = Request.Form["newest_id"];
            var previousFirstTime = Request.Form["previous_first_time"] == "value";
            var result = await _contentService.GetPreviousSearchResult(
                Model.SearchedText,
                nextToken,
                newestId,
                previousFirstTime);
            var vm = new HomeViewModel
            {
                FeederContents = await _contentService.GetHomeContent(),
                SearchContent = result
            };
            return View("Index", vm);
        }

        public async Task<IActionResult> Next()
        {
            var nextToken = Request.Form["next_token"];
            var vm = new HomeViewModel
            {
                FeederContents = await _contentService.GetHomeContent(),
                SearchContent = await _contentService.GetNextSearchResult(Model.SearchedText, nextToken)
            };
            return View("Index", vm);
        }
    }
}
